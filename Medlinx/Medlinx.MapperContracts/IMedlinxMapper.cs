﻿using System;

namespace Medlinx.MapperContracts
{
    public interface IMedlinxMapper
    {
        TDestination Map<TDestination>(object source) where TDestination : class;
    }
}
