﻿using System;
namespace Medlinx.Entities
{
    public class KeyValueStoreException : Exception
    {
        public KeyValueStoreException() { }

        public KeyValueStoreException(string message) : base(message) { }

        public KeyValueStoreException(string message, Exception innerException) : base(message, innerException) { }
    }
}
