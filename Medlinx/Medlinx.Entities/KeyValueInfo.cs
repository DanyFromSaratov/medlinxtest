﻿using System.Diagnostics;

namespace Medlinx.Entities
{
    [DebuggerDisplay("Key: {Key}. Value: {Value}")]
    public class KeyValueInfo
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
