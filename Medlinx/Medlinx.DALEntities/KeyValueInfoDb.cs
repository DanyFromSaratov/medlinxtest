﻿namespace Medlinx.DALEntities
{
    public class KeyValueInfoDb
    {
        public string Key { get; set; }

        public object Value { get; set; }
    }
}
