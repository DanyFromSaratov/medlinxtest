﻿
CREATE TABLE IF NOT EXISTS KeyValueStorage(
  Key varchar(250) NOT NULL,
  Value varchar(500) NULL,
  PRIMARY KEY (Key)
);