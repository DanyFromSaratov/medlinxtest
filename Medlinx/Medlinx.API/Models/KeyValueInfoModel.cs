﻿namespace Medlinx.API.Models
{
    public class KeyValueInfoModel
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }
}
