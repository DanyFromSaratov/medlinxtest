﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Medlinx.API.Models;
using Medlinx.Entities;
using Medlinx.LogicContracts;

using Microsoft.AspNetCore.Mvc;

namespace Medlinx.API.Controllers
{
    [ApiController]
    [Route("api/v1/keyvaluestore")]
    public class KeyValueStoreController : ControllerBase
    {
        private readonly IKeyValueStoreLogic _keyValueStoreLogic;

        public KeyValueStoreController(IKeyValueStoreLogic keyValueStoreLogic)
        {
            _keyValueStoreLogic = keyValueStoreLogic;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var keyValueInfos = await _keyValueStoreLogic.GetKeysWithExistsValuesAsync();

                return new JsonResult(new
                {
                    keyValueInfos = keyValueInfos.ToList()
                });

            }
            catch (KeyValueStoreException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("values/value")]
        public async Task<IActionResult> GetValueByKey(string key)
        {
            try
            {
                var keyValueInfo = await _keyValueStoreLogic.GetValueByKeyAsync(key);

                return new JsonResult(new
                {
                    value = keyValueInfo.Value
                });
            }
            catch(KeyValueStoreException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("values/set")]
        public async Task<IActionResult> SetValueBy(KeyValueInfoModel keyValueInfoModel)
        {
            try
            {
                var keyValueInfo = new KeyValueInfo
                {
                    Key = keyValueInfoModel?.Key,
                    Value = keyValueInfoModel?.Value
                };

                await _keyValueStoreLogic.SetValueByKeyAsync(keyValueInfo);

                return Ok();
            }
            catch (KeyValueStoreException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete]
        [Route("values/remove")]
        public async Task<IActionResult> RemoveValueByKey(string key)
        {
            try
            {
                await _keyValueStoreLogic.RemoveValueByKeyAsync(key);

                return Ok();
            }
            catch (KeyValueStoreException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    } 
}
