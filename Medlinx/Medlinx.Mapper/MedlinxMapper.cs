﻿using AutoMapper;
using Medlinx.MapperContracts;
using System;

namespace Medlinx.Mapper
{
    public class MedlinxMapper : IMedlinxMapper
    {
        public IMapper Mapper { get; }

        public MedlinxMapper(IMapper mapper)
        {
            Mapper = mapper;
        }

        public TDestination Map<TDestination>(object source) where TDestination : class
        {
           return Mapper.Map<TDestination>(source);
        }
    }
}
