﻿using AutoMapper;
using Medlinx.DALEntities;
using Medlinx.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Medlinx.Mapper.Profiles
{
    public class KeyValueStoreProfile : Profile
    {
        public KeyValueStoreProfile()
        {
            CreateKeyValueStoreMaps();
        }

        private void CreateKeyValueStoreMaps()
        {
            CreateMap<KeyValueInfo, KeyValueInfoDb>();
            CreateMap<KeyValueInfoDb, KeyValueInfo>();
        }
    }
}
