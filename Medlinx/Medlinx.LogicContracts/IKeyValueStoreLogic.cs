﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Medlinx.Entities;

namespace Medlinx.LogicContracts
{
    public interface IKeyValueStoreLogic
    {
        Task SetValueByKeyAsync(KeyValueInfo keyValueInfo);

        Task<KeyValueInfo> GetValueByKeyAsync(string key);

        Task RemoveValueByKeyAsync(string key);

        Task<IEnumerable<KeyValueInfo>> GetKeysWithExistsValuesAsync();
    }
}
