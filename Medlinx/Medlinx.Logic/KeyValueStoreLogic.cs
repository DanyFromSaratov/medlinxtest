﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Medlinx.DALContracts;
using Medlinx.Entities;
using Medlinx.LogicContracts;

namespace Medlinx.Logic
{
    public class KeyValueStoreLogic : IKeyValueStoreLogic
    {
        private readonly IKeyValueStoreDao _keyValueStoreDao;

        public KeyValueStoreLogic(IKeyValueStoreDao keyValueStoreDao)
        {
            _keyValueStoreDao = keyValueStoreDao;
        }

        public async Task<IEnumerable<KeyValueInfo>> GetKeysWithExistsValuesAsync()
        {
            var keyValueInfos = await _keyValueStoreDao
                .GetKeysWithExistsValuesAsync()
                .ConfigureAwait(false);

            return keyValueInfos.ToList();
        }

        public async Task<KeyValueInfo> GetValueByKeyAsync(string key)
        {
            var value = await _keyValueStoreDao
                .GetValueByKeyAsync(key)
                .ConfigureAwait(false);

            return value;
        }

        public async Task RemoveValueByKeyAsync(string key)
        {
            await _keyValueStoreDao
                .RemoveValueByKeyAsync(key)
                .ConfigureAwait(false);
        }

        public async Task SetValueByKeyAsync(KeyValueInfo keyValueInfo)
        {
            await _keyValueStoreDao
                .SetValueByKeyAsync(keyValueInfo)
                .ConfigureAwait(false);
        }
    }
}
