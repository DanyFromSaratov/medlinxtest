﻿using Autofac;
using Medlinx.DAL;
using Medlinx.DALContracts;
using Medlinx.Logic;
using Medlinx.LogicContracts;
using Microsoft.Extensions.Configuration;
using System;

namespace Medlinx.DI
{
    public class CommonModule : Module
    {
        private readonly IConfiguration _configuration;

        public CommonModule(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void Load(ContainerBuilder builder)
        {
            RegisterBllDependencies(builder);
            RegisterDalDependencies(builder);
        }
        
        private void RegisterBllDependencies(ContainerBuilder builder)
        {
            builder.RegisterType<KeyValueStoreLogic>()
                .As<IKeyValueStoreLogic>()
                .SingleInstance();
        }

        private void RegisterDalDependencies(ContainerBuilder builder)
        {
            var storageType = _configuration.GetSection("StorageType")["StorageType"];

            if (storageType == "Persistent")
            {
                builder.RegisterType<KeyValueStoreDao>()
                 .As<IKeyValueStoreDao>()
                 .SingleInstance();
            }
            else if (storageType == "In-Memory")
            {
                builder.RegisterType<KeyValueStoreInMemoryDao>()
                .As<IKeyValueStoreDao>()
                .SingleInstance();
            }          
        }
    }
}
