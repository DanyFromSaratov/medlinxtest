﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Medlinx.DALContracts;
using Medlinx.Entities;

namespace Medlinx.DAL
{
    public class KeyValueStoreInMemoryDao : IKeyValueStoreDao
    {
        private static readonly ConcurrentDictionary<string, string> _dictionary = new ConcurrentDictionary<string, string>();

        public async Task<IEnumerable<KeyValueInfo>> GetKeysWithExistsValuesAsync()
        {
            try
            {
                var keyValues = Task
                     .Run(() => _dictionary.Where(kv => kv.Value != null))
                     .ConfigureAwait(false)
                     .GetAwaiter()
                     .GetResult();

                var keyValueInfos = keyValues
                    .Select(kv => new KeyValueInfo
                    {
                        Key = kv.Key,
                        Value = kv.Value
                    })
                    .ToList();

                return keyValueInfos;
            }
            catch (KeyValueStoreException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<KeyValueInfo> GetValueByKeyAsync(string key)
        {
            try
            {
                var valueResult = Task
                   .Run(() =>
                   {
                       var existed = _dictionary.TryGetValue(key, out var value);
                       if (existed)
                       {
                           return value;
                       }

                       throw new KeyValueStoreException($"Не удалось получить значение по ключу = {key}");
                   })
                   .ConfigureAwait(false)
                   .GetAwaiter()
                   .GetResult();

                var keyValueInfo = new KeyValueInfo
                {
                    Key = key,
                    Value = valueResult
                };

                return keyValueInfo;
            }
            catch(KeyValueStoreException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task RemoveValueByKeyAsync(string key)
        {
            try
            {
                Task
                    .Run(() =>
                    {
                        var value = _dictionary.GetValueOrDefault(key);
                        bool tryUpdate = _dictionary.TryUpdate(key, null, value);

                        if (!tryUpdate)
                        {
                            throw new KeyValueStoreException($"Не удалось удалить значение по ключу = {key}");
                        }
                    })
                    .ConfigureAwait(false)
                    .GetAwaiter()
                    .GetResult();
            }
            catch (KeyValueStoreException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task SetValueByKeyAsync(KeyValueInfo keyValueInfo)
        {
            try
            {
                Task
                    .Run(() =>
                    {
                        var keyExisted = _dictionary.Keys.FirstOrDefault(kv => kv == keyValueInfo.Key);
                        if (keyExisted != null)
                        {
                            var value = _dictionary.GetValueOrDefault(keyValueInfo.Key);
                            bool tryUpdate = _dictionary.TryUpdate(keyValueInfo.Key, keyValueInfo.Value, value);

                            if (!tryUpdate)
                            {
                                throw new KeyValueStoreException($"Не удалось обновить значение по ключу = {keyValueInfo.Key}");
                            }
                        }
                        else
                        {
                            var tryAdd = _dictionary.TryAdd(keyValueInfo.Key, keyValueInfo.Value);

                            if (!tryAdd)
                            {
                                throw new KeyValueStoreException($"Не удалось добавить значение по ключу = {keyValueInfo.Key}");
                            }
                        }
                    })
                    .ConfigureAwait(false)
                    .GetAwaiter()
                    .GetResult();
            }
            catch (KeyValueStoreException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
