﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Medlinx.DALContracts;
using Medlinx.Entities;

using Npgsql;
using Dapper;
using Microsoft.Extensions.Configuration;

namespace Medlinx.DAL
{
    public class KeyValueStoreDao : IKeyValueStoreDao
    {
        private readonly string _connectionString;

        public KeyValueStoreDao(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("MedlinxDB");
        }

        public async Task<IEnumerable<KeyValueInfo>> GetKeysWithExistsValuesAsync()
        {
            try
            {
                var query = @"SELECT Key, Value
                            FROM KeyValueStorage
                            WHERE Value IS NOT NULL AND Value != ''";

                using (var connection = new NpgsqlConnection(_connectionString))
                {
                    var keyValueInfos = await connection
                        .QueryAsync<KeyValueInfo>(query)
                        .ConfigureAwait(false);

                    if (keyValueInfos == null)
                    {
                        return Enumerable.Empty<KeyValueInfo>();
                    }

                    return keyValueInfos;
                }
            }
            catch (Exception ex)
            {
                throw new KeyValueStoreException("Не удалось получить список данных", ex);
            }
        }

        public async Task<KeyValueInfo> GetValueByKeyAsync(string key)
        {
            try
            {
                var isKeyExists = await IsKeyExists(key);
                if (!isKeyExists)
                {
                    throw new KeyValueStoreException($"Не удалось найти значение по ключу = {key}");
                }

                var query = @"SELECT Value
                        FROM KeyValueStorage
                        WHERE Key = @Key
                        LIMIT 1";

                var parameters = new
                {
                    Key = key
                };

                using (var connection = new NpgsqlConnection(_connectionString))
                {
                    var value = await connection
                        .ExecuteScalarAsync<string>(query, parameters)
                        .ConfigureAwait(false);
                 
                    return new KeyValueInfo
                    {
                        Key = key,
                        Value = value
                    };
                }
            }
            catch(KeyValueStoreException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new KeyValueStoreException($"Не удалось удалить значение по ключу = {key}", ex);
            }
        }

        public async Task RemoveValueByKeyAsync(string key)
        {
            try
            {
                var isKeyExists = await IsKeyExists(key);
                if (!isKeyExists)
                {
                    throw new KeyValueStoreException($"Не удалось найти значение по ключу = {key}");
                }

                var query = @"UPDATE KeyValueStorage
                            SET Value = NULL
                            WHERE Key = @Key;";

                var parameters = new
                {
                    Key = key
                };

                using (var connection = new NpgsqlConnection(_connectionString))
                {
                    await connection
                        .ExecuteAsync(query, parameters)
                        .ConfigureAwait(false);
                }

            }
            catch (KeyValueStoreException ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new KeyValueStoreException($"Не удалось удалить значение по ключу = {key}", ex);
            }
        }

        public async Task SetValueByKeyAsync(KeyValueInfo keyValueInfo)
        {
            try
            {
                var query = @"UPDATE KeyValueStorage
                            SET Value = @Value
                            WHERE Key = @Key;";

                var isKeyExists = await IsKeyExists(keyValueInfo.Key);
                if (!isKeyExists)
                {
                    query = @"INSERT INTO KeyValueStorage (Key, Value) VALUES (@Key, @Value)";
                }

                var parameters = new
                {
                    Key = keyValueInfo.Key,
                    Value = keyValueInfo.Value
                };

                using (var connection = new NpgsqlConnection(_connectionString))
                {
                    await connection
                        .ExecuteAsync(query, parameters)
                        .ConfigureAwait(false);
                }
            }
            catch (Exception ex)
            {
                throw new KeyValueStoreException($"Не удалось обновить или добавить значение по ключу = {keyValueInfo.Key}", ex);
            }
        }

        public async Task<bool> IsKeyExists(string key)
        {
            try
            {
                var query = @"SELECT 1 FROM KeyValueStorage
                            WHERE Key = @Key";

                var parameters = new
                {
                    Key = key
                };

                using (var connection = new NpgsqlConnection(_connectionString))
                {
                    var value = await connection
                        .ExecuteScalarAsync<bool>(query, parameters)
                        .ConfigureAwait(false);

                    return value;
                }
            }
            catch (Exception ex)
            {
                throw new KeyValueStoreException($"Не удалось найти запись по ключу = {key}", ex);
            }
        }
    }
}
